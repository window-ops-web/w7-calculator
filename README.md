# W7 Calculator

A Windows 7-styled calculator. This fast-loading calculator is good for quick calculations, but please note that it does not currently support scientific mode.
